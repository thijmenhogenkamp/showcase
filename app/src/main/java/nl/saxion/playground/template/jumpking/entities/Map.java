package nl.saxion.playground.template.jumpking.entities;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

import nl.saxion.playground.template.jumpking.Game;
import nl.saxion.playground.template.jumpking.PlayerState;
import nl.saxion.playground.template.jumpking.entities.pickups.Coin;
import nl.saxion.playground.template.jumpking.entities.pickups.DoubleCoins;
import nl.saxion.playground.template.jumpking.entities.pickups.Jetpack;
import nl.saxion.playground.template.jumpking.entities.pickups.Pickup;
import nl.saxion.playground.template.jumpking.entities.pickups.Powerup;
import nl.saxion.playground.template.jumpking.entities.pickups.Star;
import nl.saxion.playground.template.lib.Entity;
import nl.saxion.playground.template.lib.GameView;

public class Map extends Entity {

    private static final int height = 25;
    private static final int MAX_STAR_SPACING = 210;
    private static final int MIN_STAR_SPACING = 65;
    private static final int MAX_POWERUP_SPACING = 80;
    private static final int minYDiff = 220;
    private static final int maxXDiff = 175;

    ArrayList<Platform> platforms = new ArrayList<>();
    private ArrayList<Star> stars = new ArrayList<>();
    private ArrayList<Coin> activeCoins = new ArrayList<>();

    private Pickup powerUp;
    private Powerup activePowerup;

    private int NextPUPos;
    private int nextStarPos;

    private int platformCounter;

    private Game game;
    private Character player;


    public Map(Game game, Character player) {
        this.game = game;
        this.player = player;
        generatePlatforms();
    }

    private int xBoundStart;
    private int xBoundEnd;

    private void generatePlatforms() {

        xBoundStart = (Platform.SIZE_LARGE / 2);
        xBoundEnd = (int) game.getWidth();

        int startX = (int)game.getWidth() / 2;
        int startY = (int)game.getHeight() - (int)Platform.START_OFFSET;

        // Start Platform
        platforms.add(new Platform(Platform.SIZE_LARGE, startX, startY, game));
        Random r = new Random();

        nextStarPos = MIN_STAR_SPACING + r.nextInt(MAX_STAR_SPACING - MIN_STAR_SPACING);
        NextPUPos = r.nextInt(MAX_POWERUP_SPACING);

        for(int count = 0; count < height; count++)
        {
            if(platformCounter < Integer.MAX_VALUE)
                spawnPlatform();
        }
    }

    private void spawnPlatform()
    {
        platformCounter++;
        Random r = new Random();
        Platform highest = platforms.get(platforms.size()-1);

        int newY = highest.y - minYDiff - r.nextInt(50);
        int newX;
        int size = determinePlatformSize();

        int newXLeft = (highest.x - highest.getWidth() / 2) - r.nextInt(maxXDiff);
        int newXRight = (highest.x + highest.getWidth() / 2) + r.nextInt(maxXDiff);

        // If the new X position of the platform is between the Screen's bounds, randomize the direction
        // else choose the new direction based off of which boundary is pushed

        if (newXLeft - size > xBoundStart && newXRight + size < xBoundEnd)
        {
            int dir = r.nextInt(2);
            if(dir == 0)
                newX = newXRight;
            else newX = newXLeft;
        }
        else
        {
            newX = newXLeft - size <= xBoundStart ? newXRight : newXLeft;
        }

        Platform plat = new Platform(size, newX, newY, game, platformCounter == Integer.MAX_VALUE);

        // Spawn a Star
        if(stars.size() < 3 && platformCounter == nextStarPos ) {
            Log.e("Star created:", "Platform #" + nextStarPos );
            nextStarPos = (MIN_STAR_SPACING + r.nextInt(MAX_STAR_SPACING - MIN_STAR_SPACING)) + platformCounter;
            createStarForPlatform(plat);
        }
        // Spawn powerup
        else if(platformCounter == NextPUPos)
            spawnPowerup(plat.x, plat.getTop() - 35f, r.nextInt(2));
        // Spawn coin
        else {
            spawnCoin(newX - size  / 2f,newX + size / 2f,  newY, r);
        }

        platforms.add(plat);
    }

    private void spawnCoin(float xLeft,float xRight, float newY, Random r) {
        for(int i = 0; i < r.nextInt(3) + 1; i++)
        {
            float rngX = xLeft + r.nextInt((int)xRight - (int)xLeft);
            Coin c = new Coin(rngX, newY - 50f);
            activeCoins.add(c);
        }
    }

    private void createStarForPlatform(Platform plat)
    {
        Star star = new Star(plat.x, plat.y - plat.getHeight() - 15f, Star.STAR_WIDTH, Star.STAR_HEIGHT);
        stars.add(star);
        plat.setPickup(star);
    }

    void determineNextPUPos()
    {
        Random r = new Random();
        powerUp = null;
        activePowerup = null;
        NextPUPos = r.nextInt(MAX_POWERUP_SPACING) + platformCounter;

        if(NextPUPos == nextStarPos) NextPUPos++;
    }

    private void spawnPowerup(float x, float y, int rng)
    {
        if(rng == 0)
        {
            powerUp = new Jetpack(x, y);
            activePowerup = Powerup.JETPACK;
        }
        else
        {
            powerUp = new DoubleCoins(x,y);
            activePowerup = Powerup.DOUBLECOINS;
        }

    }


    ArrayList<Star> getStars()
    {
        return stars;
    }
    Pickup getActivePU(){
        return powerUp;
    }
    ArrayList<Platform> getPlatforms() {
        return platforms;
    }
    ArrayList<Coin> getCoins()
    {
        return activeCoins;
    }

    private int determinePlatformSize()
    {
        int small = 36;
        int medium = 70;

        Random r = new Random();
        int rng = r.nextInt(101);

        if(rng <= small) return Platform.SIZE_SMALL;
        else if(rng <= medium) return Platform.SIZE_MEDIUM;
        else return Platform.SIZE_LARGE;
    }

    @Override
    public void draw(GameView gv) {
        for (Platform plat : platforms)
            if(plat.y > 0)
                plat.draw(gv);

        for(Coin c : activeCoins)
            if(c.getBottom() > 0)
                c.draw(gv);

        if(powerUp != null)
            powerUp.draw(gv);
    }

    @Override
    public void tick() {
         if(player != null){
             if(player.getCurrentState() == PlayerState.PAUSED || !player.isAlive())
                 return;
         }

        Platform toDelete = null;
        Coin coin = null;

        for (Platform plat : platforms)
        {
            if(plat.getTop() > game.getHeight())
                toDelete = plat;
            else {
                plat.tick();
            }
        }

        if(powerUp != null) {
            powerUp.tick();

            if(powerUp.getTop() > game.getHeight()) // Prepare platform for removal if under screen
                determineNextPUPos();
        }

        for(Coin c : activeCoins)
        {
            c.tick();
            if(c.getTop() > game.getHeight()) // Prepare coin for removal if under screen
                coin = c;
        }

        if(toDelete != null)
            removePlatform(toDelete);

        if(coin != null)
            removeCoin(coin);

    }

    private void removePlatform(Platform p)
    {
        platforms.remove(p);
        spawnPlatform();
    }

    private void removeCoin(Coin c)
    {
        activeCoins.remove(c);
    }

    public Powerup getActivePowerup()
    {
        return activePowerup;
    }
}
