package nl.saxion.playground.template.jumpking.entities.pickups;

import android.graphics.Bitmap;

import nl.saxion.playground.template.jumpking.entities.Character;
import nl.saxion.playground.template.jumpking.entities.Platform;
import nl.saxion.playground.template.lib.GameView;

public class Jetpack extends Pickup {
    public static int PRICE_UPGRADE = 500;
    public static final int DURATION_SECONDS = 10;
    public static final float SPEED = 6f;
    public static final int MAX_LEVEL = 3;

    private static int Level = 1;

    private static Bitmap img;
    private Character c;

    public Jetpack(float x, float y) {
        super(x, y, 70f, 70f);
    }

    @Override
    public void draw(GameView gv) {
        int angle = 0;

        if(pickedUp)
        {
            switch(c.getDirection())
            {
                case RIGHT:
                    angle = 25;
                    break;
                case LEFT:
                    angle = 360 - 25;
                    break;
                default:
                    break;
            }
        }

        gv.drawBitmap(img, getLeft(), getTop(), width,height, angle);
    }

    @Override
    public boolean checkCollisionPlayer(Character player) {
        if (super.checkCollisionPlayer(player))
        {
            c = player;
            enLarge();
            setFallSpeedsToJP();
            setPickedUp();
            return true;
        }
        return false;
    }

    @Override
    public void tick() {
        if(!isPickedUp())
            yPos += fallSpeed;
        else
        {
            xPos = c.getxPos();
            yPos = c.getyPos() + 25f;
        }
    }

    private void enLarge()
    {
        width = height = 150f;
    }

    private static void setFallSpeedsToJP()
    {
        Platform.setFallSpeed(SPEED * Level);
        Pickup.setFallSpeed(SPEED * Level);
    }

    public static int getLevel()
    {
        return Level;
    }

    public static String getPrice()
    {
        return Level == MAX_LEVEL ? "Maxed!" : "Price: " + PRICE_UPGRADE;
    }


    public static void Upgrade() {
        Level = Math.min(Level + 1, MAX_LEVEL);
        calcPrice();
    }

    public static void setLevel(int level) {
        Level = Math.min(level, MAX_LEVEL);
        calcPrice();
    }

    private static void calcPrice()
    {
        PRICE_UPGRADE = Level == 1 ? PRICE_UPGRADE : (int) (500 + (Level * 1.5f * (Level * 250)));
    }

    @Override
    public void Finish()
    {
        Platform.setFallSpeed(Platform.NORMAL_FALL_SPEED);
        Pickup.setFallSpeed(Platform.NORMAL_FALL_SPEED);
    }

    public static void setBitmap(Bitmap _img)
    {
        img = _img;
    }
}

