package nl.saxion.playground.template.jumpking;

import java.util.ArrayList;

import nl.saxion.playground.template.jumpking.entities.Background;
import nl.saxion.playground.template.jumpking.entities.Character;
import nl.saxion.playground.template.jumpking.entities.Map;
import nl.saxion.playground.template.jumpking.entities.pickups.Coin;
import nl.saxion.playground.template.jumpking.entities.pickups.Pickup;
import nl.saxion.playground.template.lib.GameModel;

public class Game extends GameModel {

    public Map map;
    private Activity act;

    public interface Listener {
        void OnCharacterCreated(Character c);

        void OnPauseChanged();

        void scoreChanged();
        void LivesChanged(int lives);
        void StarPickedUp();

        // Jetpack
        void InitiatePowerup(Pickup p);
        void UpdatePowerUp(int progress);
        void HidePowerup(Pickup p);

        // Coin
        void pickedUpCoin();
    }

    public Game(Activity act){
        super();

        this.act = act;

        Coin.setCoinValue(1);
    }

    public transient ArrayList<Listener> listeners = new ArrayList<>();

    @Override
    public void start() {
        addEntity(new Background(this));

        Character player = new Character(this);
        addEntity(player);

        map = new Map(this, player);
        addEntity(map);

        listeners.get(0).LivesChanged(player.getLives());
    }

    public Map getMap()
    {
        return map;
    }

    public void EndGame(boolean end){
        act.DrawEnd(end);
    }
}
