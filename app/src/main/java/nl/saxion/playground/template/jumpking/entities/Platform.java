package nl.saxion.playground.template.jumpking.entities;

import android.graphics.Bitmap;
import android.graphics.RectF;

import nl.saxion.playground.template.R;
import nl.saxion.playground.template.jumpking.Game;
import nl.saxion.playground.template.jumpking.entities.pickups.Pickup;
import nl.saxion.playground.template.lib.Entity;
import nl.saxion.playground.template.lib.GameView;

public class Platform extends Entity {

    static final int SIZE_LARGE = 250;
    static final int SIZE_MEDIUM = 150;
    static final int SIZE_SMALL = 100;

    public static final float NORMAL_FALL_SPEED = 1f;
    static final float START_OFFSET = 200f;

    boolean activated = false;
    boolean isLast = false;

    private int width;
    private final int height = 50;
    int x,y;
    private Pickup pickup;

    private Game game;

    private static Bitmap bitmap;
    private static float fallSpeed = NORMAL_FALL_SPEED;

    public int getHeight() {
        return height;
    }

    Platform(int size, int x, int y, Game game){
        this.width = size;
        this.x = x;
        this.y = y;
        this.game = game;
    }

    Platform(int size, int x, int y, Game game, boolean islast){
        this.width = size;
        this.x = x;
        this.y = y;
        this.game = game;
        this.isLast = islast;
    }

    void setPickup(Pickup item)
    {
        this.pickup = item;
    }

    public float getTop()
    {
        return y - height / 2f;
    }
    public float getBottom()
    {
        return y + height / 2f;
    }
    public float getLeft()
    {
        return x - width / 2f;
    }
    public float getRight()
    {
        return x + width / 2f;
    }

    public int getWidth()
    {
        return width;
    }

    @Override
    public void tick()
    {
        y += fallSpeed;

        if(pickup != null)
            pickup.tick();
    }

    @Override
    public void draw(GameView gv) {
        if(bitmap == null)
        {
            switch(width){
                case SIZE_SMALL:
                    bitmap = gv.getBitmapFromResource(R.drawable.smallpf);
                    break;
                case SIZE_MEDIUM:
                    bitmap = gv.getBitmapFromResource(R.drawable.mediumpf);
                    break;
                case SIZE_LARGE:
                    bitmap = gv.getBitmapFromResource(R.drawable.largepf);
                    break;
                default:
                    break;
            }
        }
        gv.drawBitmap(bitmap, getLeft(), getTop(), width, height);

        if(pickup != null)
            pickup.draw(gv);
    }

    void setActivated()
    {
        activated = true;
    }

    boolean checkCollision(Character player){
        RectF box = player.getHitBox();
        float maxYDiff = 4f;
        if(player.getxPos() + 5f >= getLeft() && player.getxPos() - 5f <= getRight())
        {
            return box.bottom <= getTop() + maxYDiff && box.bottom >= getTop() - maxYDiff;
        }
        return false;
    }

    public static void setFallSpeed(float fallSpeed) {
        Platform.fallSpeed = fallSpeed;
    }
}
