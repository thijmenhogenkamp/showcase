package nl.saxion.playground.template.jumpking.entities.pickups;

import android.graphics.Bitmap;
import android.util.Log;

import nl.saxion.playground.template.R;
import nl.saxion.playground.template.lib.GameView;

public class Star extends Pickup {

    public final static int STAR_POINTS = 20;

    public final static float STAR_WIDTH = 50f;
    public final static float STAR_HEIGHT = 50f;

    boolean finishedAnimation;

    private static Bitmap img;

    int rotation = 0;

    double lastTime;

    public Star(float x, float y, float width, float height) {
        super(x, y, width, height);
    }

    @Override
    public void setPickedUp() {
        super.setPickedUp();

        lastTime = System.currentTimeMillis();
    }

    @Override
    public void draw(GameView gv) {
        if(img == null)
        {
            img = gv.getBitmapFromResource(R.drawable.starsmall);
        }
        else if(!finishedAnimation)
            gv.drawBitmap(img, xPos - width/2f, yPos - height /2f, width,height, rotation);
    }

    @Override
    public void Finish() {
    }

    @Override
    public void tick() {
        super.tick();


        // Pickup Animation :)
        if(pickedUp && !finishedAnimation)
        {
            double dt = System.currentTimeMillis() - lastTime;
            dt /= 1000;
            lastTime = System.currentTimeMillis();

            rotation += 180 * dt;
            width = height += 15 * dt;

            if(rotation >= 360)
                finishedAnimation = true;
        }
        else lastTime = System.currentTimeMillis();
    }
}
