package nl.saxion.playground.template;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;


import nl.saxion.playground.template.jumpking.Activity;
import nl.saxion.playground.template.jumpking.UpgradesActivity;
import nl.saxion.playground.template.jumpking.entities.Character;
import nl.saxion.playground.template.jumpking.entities.pickups.DoubleCoins;
import nl.saxion.playground.template.jumpking.entities.pickups.Jetpack;

public class MainActivity extends AppCompatActivity {

    TextView tvHighscore, tvCoins;
    Switch autoJump;
    SharedPreferences prefs;
    ImageView imgUpgrades;

    MediaPlayer player;

    int score = -1;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jumkingmenu);

        ImageButton startBtn = findViewById(R.id.playButton);
        tvHighscore = findViewById(R.id.highScore);
        tvCoins = findViewById(R.id.txtCoins);
        autoJump  = findViewById(R.id.swAutoJump);
        imgUpgrades = findViewById(R.id.imgUpgrades);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGame();
            }
        });
        imgUpgrades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), UpgradesActivity.class);
                startActivity(i);
            }
        });

        prefs = getSharedPreferences(getString(R.string.prefs_key), MODE_PRIVATE);

        int highScore = prefs.getInt(getString(R.string.highscore_key), 0);

        tvHighscore.setText(""+highScore);
        tvCoins.setText("" + prefs.getInt(getString(R.string.coins_key),0));

        Jetpack.setLevel(prefs.getInt(getString(R.string.jetpack_level), 1));
        DoubleCoins.setLevel(prefs.getInt(getString(R.string.doublecoins_level), 1));
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();
        startMusic();
        tvCoins.setText("" + prefs.getInt(getString(R.string.coins_key),0));
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.stop();
        player.release();
    }

    private void startMusic()
    {
        player = MediaPlayer.create(this, R.raw.title);
        player.setVolume(1,1);
        player.setLooping(true);
        player.start();
    }

    void startGame()
    {
        Character.AutoJump = autoJump.isChecked();

        Intent i = new Intent(this, Activity.class);
        startActivity(i);
    }

}
