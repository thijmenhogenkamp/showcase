package nl.saxion.playground.template.jumpking;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import nl.saxion.playground.template.R;
import nl.saxion.playground.template.jumpking.entities.pickups.DoubleCoins;
import nl.saxion.playground.template.jumpking.entities.pickups.Jetpack;

@SuppressLint("SetTextI18n")
public class UpgradesActivity extends AppCompatActivity {

    SharedPreferences prefs;

    RadioButton jpLevel1, jpLevel2, jpLevel3, coinLv1, coinLv2, coinLv3;
    ImageView jpBuy, coinBuy;
    TextView txtCoins, txtCoinPrice, txtJpPrice;

    int coins = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrades);

        setViews();

        prefs = getSharedPreferences(getString(R.string.prefs_key), 0);

        coins = prefs.getInt(getString(R.string.coins_key), 0);

        txtCoins.setText("" + coins);
        txtJpPrice.setText(Jetpack.getPrice());
        txtCoinPrice.setText(DoubleCoins.getPrice());

        setRadioLevels();
    }

    private void setViews()
    {
        jpLevel1 = findViewById(R.id.jpLevel1);
        jpLevel2 = findViewById(R.id.jpLevel2);
        jpLevel3 = findViewById(R.id.jpLevel3);
        jpBuy = findViewById(R.id.jpBuy);

        coinLv1 = findViewById(R.id.coinLevel1);
        coinLv2 = findViewById(R.id.coinLevel2);
        coinLv3 = findViewById(R.id.coinLevel3);
        coinBuy = findViewById(R.id.coinBuy);

        txtCoinPrice = findViewById(R.id.coinPrice);
        txtJpPrice = findViewById(R.id.jpPrice);

        jpBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upgradeJetPack();
            }
        });
        coinBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upgradeDoubleCoin();
            }
        });

        txtCoins = findViewById(R.id.txtCoins);
    }

    private void upgradeDoubleCoin() {
        if(coins >= DoubleCoins.PRICE_UPGRADE && DoubleCoins.getLevel() < DoubleCoins.MAX_LEVEL)
        {
            coins -= DoubleCoins.PRICE_UPGRADE;
            DoubleCoins.Upgrade();
            saveCoins();
        }
        else {
            String message = DoubleCoins.getLevel() == DoubleCoins.MAX_LEVEL ? "Maxed out!" : "You dont have the required amount of Coins!\nNeed " + (DoubleCoins.PRICE_UPGRADE - coins)  + " more";
            Toast.makeText(this, message , Toast.LENGTH_LONG).show();
        }
    }

    void upgradeJetPack()
    {
        if(coins >= Jetpack.PRICE_UPGRADE && Jetpack.getLevel() < Jetpack.MAX_LEVEL)
        {
            coins -= Jetpack.PRICE_UPGRADE;
            Jetpack.Upgrade();
            saveCoins();
        }
        else {
            String message = Jetpack.getLevel() == Jetpack.MAX_LEVEL ? "Maxed out!" : "You dont have the required amount of Coins!\nNeed " + (Jetpack.PRICE_UPGRADE - coins)  + " more";
            Toast.makeText(this, message , Toast.LENGTH_LONG).show();
        }
    }

    void saveCoins()
    {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(getString(R.string.coins_key), coins);
        saveLevels(edit);
        edit.apply();

        txtCoins.setText("" + coins);

        setRadioLevels();
        txtJpPrice.setText(Jetpack.getPrice());
        txtCoinPrice.setText(DoubleCoins.getPrice());
    }

    void setRadioLevels()
    {
        switch(DoubleCoins.getLevel()) {
            case 1:
                coinLv1.setChecked(true);
                break;
            case 2:
                coinLv1.setChecked(true);
                coinLv2.setChecked(true);
                break;
            case 3:
                coinLv1.setChecked(true);
                coinLv2.setChecked(true);
                coinLv3.setChecked(true);
                break;
            default:
                break;
        }
        switch(Jetpack.getLevel())
        {
            case 1:
                jpLevel1.setChecked(true);
                break;
            case 2:
                jpLevel1.setChecked(true);
                jpLevel2.setChecked(true);
                break;
            case 3:
                jpLevel1.setChecked(true);
                jpLevel2.setChecked(true);
                jpLevel3.setChecked(true);
                break;
            default:
                break;
        }
    }

    void saveLevels(SharedPreferences.Editor edit)
    {
        edit.putInt(getString(R.string.jetpack_level), Jetpack.getLevel());
        edit.putInt(getString(R.string.doublecoins_level), DoubleCoins.getLevel());
    }

}


