package nl.saxion.playground.template.jumpking.entities;

public enum Direction
{
    LEFT, RIGHT, NONE;
}
