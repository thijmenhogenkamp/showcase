package nl.saxion.playground.template.jumpking;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import nl.saxion.playground.template.MainActivity;
import nl.saxion.playground.template.R;
import nl.saxion.playground.template.jumpking.entities.pickups.Star;

public class ScoreEvaluation extends AppCompatActivity {

    int starsGotten = 0;
    int totalScore = 0;

    int finalScore = 0;
    int curScore = 0;
    int curId = 0;

    TextView txtScore;
    ImageView star1, star2, star3;
    Button btnEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score_evaluation);

        Intent i = getIntent();
        starsGotten = i.getIntExtra(getString(R.string.starCountKey), 0);
        totalScore = i.getIntExtra(getString(R.string.scoreKey), 0);

        Log.e("stars:", ""+starsGotten);
        Log.e("score:", ""+totalScore);

        star1 = findViewById(R.id.star1);
        star2 = findViewById(R.id.star2);
        star3 = findViewById(R.id.star3);
        txtScore = findViewById(R.id.txtScore);
        btnEnd = findViewById(R.id.btnEnd);

        finalScore = totalScore + (starsGotten * Star.STAR_POINTS);

        btnEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ScoreEvaluation.this, MainActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        final ValueAnimator scoreAnim = ValueAnimator.ofInt(curScore, finalScore);
        scoreAnim.setInterpolator(new LinearInterpolator());
        scoreAnim.setDuration(1500);
        scoreAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                curScore = (int) valueAnimator.getAnimatedValue();
                txtScore.setText(String.valueOf(curScore));
            }
        });

        final ValueAnimator alphaAnim = ValueAnimator.ofFloat(0f, 1f);
        alphaAnim.setDuration(starsGotten > 0 ? 1500 / starsGotten : 1500);
        alphaAnim.setRepeatCount(starsGotten-1);
        alphaAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float val = (float) valueAnimator.getAnimatedValue();
                getCurrentStar().setAlpha(val);
            }
        });
        alphaAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                curId++;
            }

            @Override
            public void onAnimationEnd(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {
                curId++;
            }
        });

        scoreAnim.start();
        if(starsGotten > 0)
            alphaAnim.start();
    }

    private ImageView getCurrentStar()
    {
        switch (curId){
            case 1:
                return star1;
            case 2:
                return star2;
            default:
                return star3;
        }

    }
}
