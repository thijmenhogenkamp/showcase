package nl.saxion.playground.template.jumpking.entities;

import android.graphics.Bitmap;
import android.graphics.RectF;

import nl.saxion.playground.template.R;
import nl.saxion.playground.template.jumpking.Game;
import nl.saxion.playground.template.jumpking.PlayerState;
import nl.saxion.playground.template.jumpking.entities.pickups.Coin;
import nl.saxion.playground.template.jumpking.entities.pickups.DoubleCoins;
import nl.saxion.playground.template.jumpking.entities.pickups.Jetpack;
import nl.saxion.playground.template.jumpking.entities.pickups.Pickup;
import nl.saxion.playground.template.jumpking.entities.pickups.Powerup;
import nl.saxion.playground.template.jumpking.entities.pickups.Star;
import nl.saxion.playground.template.lib.Entity;
import nl.saxion.playground.template.lib.GameView;

public class Character extends Entity {

    private static final float X_SPEED = 1.5f;

    public static boolean AutoJump;


    private final float height = 150f;
    private final float width = 90f;
    private final float gravity = 0.1f;
    private final float jumpHeight = 8f;
    private final float maxFallSpeed = -6f;

    private float xPos, yPos, ySpeed;

    private int lives = 3;
    private int lastDrawn;
    private Direction xDirection = Direction.NONE;

    private Game game;

    private static Bitmap bm_right;
    private static Bitmap bm_left;


    private PlayerState currentState = PlayerState.PAUSED;
    private Platform lastPlatform;

    private Pickup activePowerup;
    private double powerupTime;

    private double lastTime = System.currentTimeMillis();
    private boolean onPlatform = true;
    private boolean hasJetPack = false;


    public Character(Game game)
    {
        this.game = game;
        this.xPos = game.getWidth() / 2;
        this.yPos = game.getHeight() - Platform.START_OFFSET - height / 2 - 25f;

        game.listeners.get(0).OnCharacterCreated(this);
    }

    public RectF getHitBox()
    {
        return new RectF(xPos - width / 2f, yPos - height / 2f, xPos + width / 2,yPos + height / 2f );
    }


    public void Jump(){
        if(!isAlive())
            return;

        if(onPlatform){

            if(isPaused())
                UnPause();

            currentState = PlayerState.JUMPING;
            ySpeed = jumpHeight;
        }
    }

    @Override
    public void tick()
    {
        onPlatform = false;

        double dt = System.currentTimeMillis() - lastTime;
        lastTime = System.currentTimeMillis();

        if(currentState != PlayerState.PAUSED && isAlive())
        {

            if(activePowerup != null)
            {
                powerupTime += dt;
                updatePowerUpTimer();
            }

            if(!hasJetPack)
                yPos -= ySpeed;

            switch(xDirection)
            {
                case RIGHT: // Right
                    if(!isGonnaTouchWall())
                        xPos += X_SPEED;
                    break;
                case LEFT: // Left
                    if(!isGonnaTouchWall())
                        xPos -= X_SPEED;
                    break;
                default:
                    break;
            }


            boolean collided = false;
            if(ySpeed <= 0 || hasJetPack)
            {
                for(Platform p : game.map.platforms)
                {
                    if(hasJetPack)
                    {
                        if(p.getTop() > yPos && !p.activated)
                        {
                            addPoint();
                            p.setActivated();
                        }
                    }
                    else if(p.getTop() < 0) break;
                    else if(p.checkCollision(this)) {
                        if(!p.activated){
                            addPoint();
                            p.setActivated();
                        }
                        if(p.isLast){
                            EndGame(true);
                        }
                        lastPlatform = p;

                        onPlatform = true;

                        if(AutoJump)
                            Jump();
                        else ySpeed = -Platform.NORMAL_FALL_SPEED;

                        collided = true;
                        break;
                    }
                    else {
                        currentState = PlayerState.FALLING;
                    }
                }
            }

            if(!collided && ySpeed > maxFallSpeed && !hasJetPack)
                ySpeed -= gravity;

            if(yPos > game.getHeight() - height / 2){
                DeductLife();
            }
        }

        for(Star star : game.map.getStars())
        {
            if(!star.isPickedUp() && (star.checkCollisionPlayer(this) || (hasJetPack && star.getYPos() > yPos) )){
                star.setPickedUp();
                game.listeners.get(0).StarPickedUp();
            }
        }

        for(Coin c : game.getMap().getCoins())
        {
            if(c.checkCollisionPlayer(this) || (hasJetPack && c.getYPos() > yPos))
            {
                c.pickedUpCoin(this);
            }
        }

        if(activePowerup == null && game.getMap().getActivePU() != null  && game.getMap().getActivePU().checkCollisionPlayer(this))
        {
            pickupPowerup();
        }

    }

    private boolean isGonnaTouchWall()
    {
        return xPos - X_SPEED <= width / 2 || xPos + X_SPEED >= game.getWidth() - width/2;
    }

    private void updatePowerUpTimer()
    {
        game.listeners.get(0).UpdatePowerUp((int) powerupTime);
        if(powerupTime / 1000 > (hasJetPack ? Jetpack.DURATION_SECONDS : DoubleCoins.DURATION_SECONDS))
        {
            resetPowerup();

            if(hasJetPack)
            {
                ySpeed = Jetpack.SPEED * 1.5f;
                hasJetPack = false;
            }
        }
    }

    private void StopPlayer()
    {
        ySpeed = 0;
        xDirection = Direction.NONE;
        Pause();
    }

    public boolean isPaused()
    {
        return currentState == PlayerState.PAUSED;
    }

    public void TogglePause()
    {
        if(isPaused())
        {
            UnPause();
        }
        else {
            Pause();
        }
    }

    public void Pause()
    {
        currentState = PlayerState.PAUSED;
        game.listeners.get(0).OnPauseChanged();
    }

    public void UnPause()
    {
        currentState = PlayerState.FALLING;
        game.listeners.get(0).OnPauseChanged();
    }

    private void setPosition(float x, float y)
    {
        xPos = x;
        yPos = y - height / 2f;
    }

    public float getxPos() {
        return xPos;
    }

    public float getyPos() {
        return yPos;
    }

    public int getLives() {
        return lives;
    }

    public PlayerState getCurrentState() {
        return currentState;
    }

    private void DeductLife()
    {
        lives -= 1;

        game.listeners.get(0).LivesChanged(lives);

        if(lastPlatform == null || lastPlatform.y + 50 >= game.getHeight())
        {
           lastPlatform = searchNewPlatform();
        }

        if(lastPlatform != null)
            setPosition(lastPlatform.x, lastPlatform.getTop());

        StopPlayer();

        if(lives == 0)
            EndGame(false);
    }

    private void addPoint()
    {
        for(Game.Listener listener : game.listeners)
        {
            listener.scoreChanged();
        }
    }

    public void pickedUpCoin()
    {
        game.listeners.get(0).pickedUpCoin();
    }

    public boolean isAlive()
    {
        return currentState != PlayerState.DEAD;
    }

    public boolean PowerupIsJetpack()
    {
        return activePowerup != null && activePowerup instanceof Jetpack;
    }

    @Override
    public void draw(GameView gv)
    {

        if(bm_right == null)
            bm_right = gv.getBitmapFromResource(R.drawable.knight_right);
        if(bm_left == null)
            bm_left = gv.getBitmapFromResource(R.drawable.knight_left);

        switch(xDirection)
        {
            case RIGHT:
                gv.drawBitmap(bm_right, xPos-width / 2, yPos-height / 2, width, height, getRightAngle());
                lastDrawn = 1;
                break;
            case LEFT:
                gv.drawBitmap(bm_left, xPos-width / 2, yPos-height / 2, width, height, getLeftAngle() );
                lastDrawn = 2;
                break;
            default:
                if(lastDrawn == 2)
                    gv.drawBitmap(bm_left, xPos-width / 2, yPos-height / 2, width, height, 0);
                else
                    gv.drawBitmap(bm_right, xPos-width / 2, yPos-height / 2, width, height, 0);
                break;
        }

    }

    private Platform searchNewPlatform()
    {
        for (Platform p : game.getMap().getPlatforms()){
            if(p != null && p.y < game.getHeight() - 50)
                return p;
        }
        return null;
    }

    private void EndGame(boolean endReached){
        currentState = PlayerState.DEAD;
        game.EndGame(endReached);
    }

    private int getLeftAngle()
    {
        return hasJetPack ? 360-25 : 0;
    }
    private int getRightAngle()
    {
        return hasJetPack ? 25 : 0;
    }
    public void setXDirection(Direction dir)
    {
        if(!isAlive())
            return;

        if(currentState == PlayerState.PAUSED)
        {
            currentState = PlayerState.FALLING;
            game.listeners.get(0).OnPauseChanged();
        }

        xDirection = dir;
    }

    public Direction getDirection()
    {
        return xDirection;
    }

    private void resetPowerup()
    {
        game.listeners.get(0).HidePowerup(activePowerup);

        activePowerup.Finish();
        activePowerup = null;

        game.getMap().determineNextPUPos();

    }

    private void pickupPowerup()
    {
        activePowerup = game.getMap().getActivePU();
        powerupTime = 0;

        if(game.getMap().getActivePowerup() == Powerup.JETPACK)
        {
            ySpeed = 0;
            yPos = game.getHeight() / 2 + height;
            hasJetPack = true;
        }
        else Coin.setCoinValue(2);

        game.listeners.get(0).InitiatePowerup(activePowerup);
    }

    @Override
    public int getLayer() {
        return 1;
    }



}

