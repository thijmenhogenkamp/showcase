package nl.saxion.playground.template.jumpking.entities.pickups;

import android.graphics.Bitmap;

import nl.saxion.playground.template.R;
import nl.saxion.playground.template.jumpking.entities.Character;
import nl.saxion.playground.template.lib.GameView;

public class Coin extends Pickup {

    private static final float COIN_WIDTH = 30f;
    private static final float COIN_HEIGHT = 30f;

    private static Bitmap img;
    private static int value = 1;

    public Coin(float x, float y) {
        super(x, y, COIN_WIDTH, COIN_HEIGHT);
    }

    @Override
    public boolean checkCollisionPlayer(Character player) {
        return super.checkCollisionPlayer(player);
    }

    @Override
    public void draw(GameView gv) {
        if(img == null)
        {
            img = gv.getBitmapFromResource(R.drawable.coin);
        }

        if(!isPickedUp())
            gv.drawBitmap(img, xPos - COIN_WIDTH / 2, yPos - COIN_HEIGHT / 2, COIN_WIDTH, COIN_HEIGHT);
    }

    @Override
    public void Finish() {
    }

    public static void setCoinValue(int invalue)
    {
        value = invalue;
    }

    public static int getValue()
    {
        return value;
    }

    public void pickedUpCoin(Character player)
    {
        if(isPickedUp())
            return;

        setPickedUp();
        player.pickedUpCoin();
    }
}
