package nl.saxion.playground.template.jumpking.customviews;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

public class RainbowText extends View {

    String value = String.valueOf(0);
    int currColor;
    Paint rainbowPaint;

    float[] hsv = new float[]{0f, 100f, 100f};

    public RainbowText(Context context) {
        super(context);

        init();
    }

    public RainbowText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    void init(){
        currColor = Color.HSVToColor(hsv);
        rainbowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        rainbowPaint.setColor(currColor);
        rainbowPaint.setTextSize(50);
        rainbowPaint.setTextAlign(Paint.Align.RIGHT);
    }

    public void setText(String text)
    {
        this.value = text;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float centerY = getMeasuredHeight() / 2f;

        rainbowPaint.setTextSize(getHeight() / 2f);

        canvas.drawText(value, getWidth(), centerY, rainbowPaint);
    }

    public void startAnimation()
    {
        final TimeInterpolator newInterpolator = new LinearInterpolator();
        final ValueAnimator animator = ValueAnimator.ofFloat(0f, 360f);

        animator.setDuration(6000);
        animator.setInterpolator(newInterpolator);
        animator.setRepeatMode(ValueAnimator.RESTART);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                hsv[0] = (float) animation.getAnimatedValue();
                rainbowPaint.setColor(Color.HSVToColor(hsv));
                invalidate();
            }
        });
        animator.start();
    }

}
