package nl.saxion.playground.template.jumpking;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import nl.saxion.playground.template.R;
import nl.saxion.playground.template.jumpking.customviews.RainbowText;
import nl.saxion.playground.template.jumpking.entities.Character;
import nl.saxion.playground.template.jumpking.entities.Direction;
import nl.saxion.playground.template.jumpking.entities.pickups.Coin;
import nl.saxion.playground.template.jumpking.entities.pickups.DoubleCoins;
import nl.saxion.playground.template.jumpking.entities.pickups.Jetpack;
import nl.saxion.playground.template.jumpking.entities.pickups.Pickup;
import nl.saxion.playground.template.jumpking.entities.pickups.Star;
import nl.saxion.playground.template.lib.GameView;

public class Activity extends AppCompatActivity implements Game.Listener {

    Game game;
    GameView gameView;
    TextView tvEnd, txtCoinCounter, txtPause;
    Button btnStop, btnJump;
    RainbowText rgbScore;
    ImageView imgStar1, imgStar2, imgStar3, imgHeart1, imgHeart2, imgHeart3, imgPause;
    ProgressBar PowerupBar;
    SeekBar controlBar;

    private MediaPlayer bgMusic, jetPack, coinPlayer;
    private SharedPreferences prefs;
    Character character;

    int coinCounter = 0;
    int scoreCounter = 0;
    int starCounter = 0;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_jumpking);

        setViews();

        tvEnd.setEnabled(false);
        btnStop.setEnabled(false);
        tvEnd.setVisibility(View.GONE);
        btnStop.setVisibility(View.GONE);
        PowerupBar.setVisibility(View.GONE);

        initializeDrawables();

        if (savedInstanceState!=null && savedInstanceState.containsKey("game")) {
            game = (Game)savedInstanceState.getSerializable("game");
        } else {
            game = new Game(this);
        }

        gameView.setGame(game);
        game.listeners.add(this);

        btnJump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(character!= null)
                    character.Jump();
            }
        });

        imgPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(character!=null) {

                    character.TogglePause();

                    handlePause();

                }
            }
        });

        controlBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(character == null)
                    return;

                if(seekBar.getProgress() > controlBar.getMax() /  2)
                    character.setXDirection(Direction.RIGHT);
                else if (seekBar.getProgress() < controlBar.getMax() / 2)
                    character.setXDirection(Direction.LEFT);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(character != null)
                    character.setXDirection(Direction.NONE);
            }
        });

        rgbScore.startAnimation();
        jetPack = MediaPlayer.create(this, R.raw.jetpack);
        coinPlayer = MediaPlayer.create(this, R.raw.pickup);

        prefs = getSharedPreferences(getString(R.string.prefs_key), MODE_PRIVATE);

        txtCoinCounter.setText(""+coinCounter);
    }

    private void handlePause() {
        controlBar.setEnabled(!character.isPaused());
        gameView.setAlpha(character.isPaused() ? .5f : 1f);
        txtPause.setVisibility(character.isPaused() ? View.VISIBLE : View.GONE);
        imgPause.setImageResource(character.isPaused() ? R.drawable.ic_play : R.drawable.ic_pause);

        if(character.PowerupIsJetpack())
        {
            if(character.isPaused())
            {
                jetPack.pause();
            }
            else jetPack.start();
        }
    }

    private void setViews()
    {
        gameView = findViewById(R.id.jumpkingGV);
        rgbScore = findViewById(R.id.rgbScore);
        tvEnd = findViewById(R.id.tvEnd);
        txtCoinCounter  =findViewById(R.id.txtCoinCounter);
        txtPause = findViewById(R.id.txtPaused);
        btnStop = findViewById(R.id.btnStop);
        imgPause = findViewById(R.id.imgPause);

        imgHeart1 = findViewById(R.id.imgHeart1);
        imgHeart2 = findViewById(R.id.imgHeart2);
        imgHeart3 = findViewById(R.id.imgHeart3);

        imgStar1 = findViewById(R.id.imgStar1);
        imgStar2 = findViewById(R.id.imgStar2);
        imgStar3 = findViewById(R.id.imgStar3);

        btnJump = findViewById(R.id.btnJump);

        PowerupBar = findViewById(R.id.jetPackTime);
        controlBar = findViewById(R.id.seekControls);
    }


    @Override
    protected void onResume() {
        super.onResume();
        startMusic();
    }

    @Override
    protected void onPause() {
        super.onPause();
        bgMusic.stop();
        bgMusic.release();
        jetPack.release();
    }

    @Override
    public void onBackPressed() {
        character.Pause();
        new AlertDialog.Builder(this)
                .setTitle("End Game")
                .setMessage("Are you sure you want to end the game?")
                .setIcon(R.drawable.ic_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        End();
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void startMusic()
    {
        bgMusic = MediaPlayer.create(this, R.raw.game);
        bgMusic.setVolume(.6f,.6f);
        bgMusic.setLooping(true);
        bgMusic.start();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("game", game);
    }

    @Override
    public void StarPickedUp()
    {
        starCounter++;
        switch(starCounter) {
            case 1:
                imgStar1.setVisibility(View.VISIBLE);
                break;
            case 2:
                imgStar2.setVisibility(View.VISIBLE);
                break;
            case 3:
                imgStar3.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private void setCurrentHearts(int lives)
    {
        switch(lives)
        {
            case 0:
                imgHeart1.setVisibility(View.INVISIBLE);
                break;
            case 1:
                imgHeart2.setVisibility(View.INVISIBLE);
                break;
            case 2:
                imgHeart3.setVisibility(View.INVISIBLE);
                break;
            default:
                break;
        }
    }

    @Override
    public void InitiatePowerup(Pickup p) {
        PowerupBar.setVisibility(View.VISIBLE);
        PowerupBar.setProgress(0);

        if(p instanceof Jetpack)
        {
            jetPack.start();
            PowerupBar.setMax(Jetpack.DURATION_SECONDS * 1000);
        }
        else {
            PowerupBar.setMax(DoubleCoins.DURATION_SECONDS * 1000);
        }
    }

    @Override
    public void UpdatePowerUp(int progress) {
        PowerupBar.setProgress(progress);
    }

    @Override
    public void HidePowerup(Pickup p) {
        PowerupBar.setVisibility(View.GONE);

        if(p instanceof Jetpack)
            jetPack.stop();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void pickedUpCoin() {
        coinCounter += Coin.getValue();
        txtCoinCounter.setText( "" + coinCounter);

        if(coinPlayer.isPlaying())
            coinPlayer.seekTo(0);
        else coinPlayer.start();
    }

    private void saveStats()
    {
        int curHighscore = prefs.getInt(getString(R.string.highscore_key), 0);
        int newScore = scoreCounter + starCounter * Star.STAR_POINTS;
        int curCoins = prefs.getInt(getString(R.string.coins_key), 0) + coinCounter;

        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(getString(R.string.coins_key), curCoins);
        edit.putInt(getString(R.string.highscore_key), Math.max(newScore, curHighscore));
        edit.apply();
    }

    @Override
    public void OnCharacterCreated(Character c) {
        this.character = c;
    }

    @Override
    public void OnPauseChanged() {
        handlePause();
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    public void scoreChanged()
    {
        scoreCounter++;
        rgbScore.setText(""+scoreCounter);
    }

    @Override
    public void LivesChanged(int lives)
    {
        setCurrentHearts(lives);
    }

    void DrawEnd(boolean endReached){

        tvEnd.setText(endReached ? getString(R.string.reached_end) : getString(R.string.you_died) + " " + scoreCounter);

        tvEnd.setEnabled(true);
        btnStop.setEnabled(true);
        tvEnd.setVisibility(View.VISIBLE);
        btnStop.setVisibility(View.VISIBLE);
        gameView.setAlpha(.5f);

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                End();
            }
        });
    }

    void End(){
        saveStats();
        game.listeners.remove(this);
        game = null;
        Intent i = new Intent(this, ScoreEvaluation.class);
        i.putExtra(getString(R.string.starCountKey), starCounter);
        i.putExtra(getString(R.string.scoreKey), scoreCounter);
        startActivity(i);
        finish();
    }

    void initializeDrawables()
    {
        Jetpack.setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.jetpack));
    }
}
