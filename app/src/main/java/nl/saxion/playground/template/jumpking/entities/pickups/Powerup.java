package nl.saxion.playground.template.jumpking.entities.pickups;

public enum Powerup {
    JETPACK, DOUBLECOINS;
}
