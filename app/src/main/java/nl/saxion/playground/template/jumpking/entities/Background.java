package nl.saxion.playground.template.jumpking.entities;

import android.graphics.Bitmap;
import nl.saxion.playground.template.R;
import nl.saxion.playground.template.jumpking.Game;
import nl.saxion.playground.template.lib.Entity;
import nl.saxion.playground.template.lib.GameView;

public class Background extends Entity {

    private static Bitmap bitmap;

    private nl.saxion.playground.template.jumpking.Game game;

    public Background(Game game) {
        this.game = game;
    }

    @Override
    public void draw(GameView gv) {
        if (bitmap==null) {
            bitmap = gv.getBitmapFromResource(R.drawable.background);
        }

        gv.drawBitmap(bitmap, 0, 0, game.getWidth(), game.getHeight());
    }
}