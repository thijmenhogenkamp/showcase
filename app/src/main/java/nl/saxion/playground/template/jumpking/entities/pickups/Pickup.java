package nl.saxion.playground.template.jumpking.entities.pickups;

import android.graphics.RectF;

import nl.saxion.playground.template.jumpking.entities.Character;
import nl.saxion.playground.template.jumpking.entities.Platform;
import nl.saxion.playground.template.lib.Entity;
import nl.saxion.playground.template.lib.GameView;

public abstract class Pickup extends Entity {

    protected float xPos, yPos, width, height;
    protected boolean pickedUp = false;

    static float fallSpeed = Platform.NORMAL_FALL_SPEED;

    public boolean isPickedUp() {
        return pickedUp;
    }

    Pickup(float x, float y, float width, float height)
    {
        xPos = x;
        yPos = y;
        this.width = width;
        this.height = height;
    }

    public void setPickedUp()
    {
        pickedUp = true;
    }

    public boolean checkCollisionPlayer(Character player) {
        RectF pHitbox = player.getHitBox();

        return((getLeft() > pHitbox.left && getLeft() < pHitbox.right) || (getRight() > pHitbox.left && getRight() < pHitbox.right))
                && ((getTop() > pHitbox.top && getTop() < pHitbox.bottom) || (getBottom() < pHitbox.bottom && getBottom() > pHitbox.top));
    }

    public static void setFallSpeed(float ySpeed)
    {
        fallSpeed = ySpeed;
    }

    @Override
    public void tick() {
        yPos += fallSpeed;
    }

    public float getYPos() {
        return yPos;
    }

    public float getTop()
    {
        return yPos - height / 2;
    }
    public float getBottom()
    {
        return yPos + height / 2;
    }
    public float getLeft()
    {
        return xPos - width / 2f;
    }
    public float getRight()
    {
        return xPos + width / 2f;
    }

    @Override
    public abstract void draw(GameView gv);

    public abstract void Finish();
}
