package nl.saxion.playground.template.jumpking;

public enum PlayerState {
    JUMPING, STILL, PAUSED, DEAD, FALLING;
}
