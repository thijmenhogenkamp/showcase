package nl.saxion.playground.template.jumpking.entities.pickups;

import android.graphics.Bitmap;

import nl.saxion.playground.template.R;
import nl.saxion.playground.template.jumpking.entities.Character;
import nl.saxion.playground.template.lib.GameView;

public class DoubleCoins extends Pickup {
    public static int PRICE_UPGRADE = 630;
    public static int DURATION_SECONDS = 10;
    public static final int MAX_LEVEL = 3;

    private final static float DIMS = 70f;
    private static int Level = 1;

    private static Bitmap img;

    public DoubleCoins(float x, float y) {
        super(x, y, DIMS, DIMS);
    }

    @Override
    public void draw(GameView gv) {
        if(img == null)
        {
            img = gv.getBitmapFromResource(R.drawable.doublecoin);
        }
        if(!isPickedUp())
            gv.drawBitmap(img, getLeft(), getTop(), DIMS, DIMS);
    }

    @Override
    public boolean checkCollisionPlayer(Character player) {
        if(super.checkCollisionPlayer(player))
        {
            setPickedUp();
            return true;
        }
        return false;
    }

    public static int getLevel()
    {
        return Level;
    }

    public static String getPrice()
    {
        return Level == MAX_LEVEL ? "Maxed!" : "Price: " + PRICE_UPGRADE;
    }

    public static void Upgrade() {
        setLevel(Level + 1);
    }

    public static void setLevel(int level) {
        Level = Math.min(level, MAX_LEVEL);
        calcPrice();
        DURATION_SECONDS = 15 + (Level * 10);
    }

    private static void calcPrice()
    {
        PRICE_UPGRADE = Level == 1 ? PRICE_UPGRADE : (int) (630 + (Level * 1.5f * (Level * 250)));
    }

    @Override
    public void Finish()
    {
        Coin.setCoinValue(1);
    }
}
